##############################################################
Sequential Streaming Networked Atomic Firmware Updater (snafu)
##############################################################


Intro
#####

`snafu` provides building blocks for designing customized OTA
firmware updaters.

The design choices are:

- Mainly consider fail-safe upgrade mechanisms,
  ie. active/inactive slots, and stay fail-safe.

- Zero-copy, in the sense that the data is only written once
  to its final location on mass storage media.

  See `Limitations`_.

- Modular code, with plug-ins that can be used
  to compose appropriate upgrade mechanisms.

- Reduced footprint of the firmware updater on a typical
  embedded Linux system.

- Do not constrain upgrade package structure;
  we include helpers to process common things
  like tarball archives, and CMS-signed payloads.

- Favor simple OTA package formats.

`snafu` is intended to be running as an HTTP service,
but the code can be reused in a way that it is a
spawned process.


Usage
#####

Write your own platform plug-in, that can reuse existing code;
you can also submit helper code for possible upstreaming.

Only install the files you need.

Run the HTTP server with your plugin name as argument.

A simple OTA upgrade package can be a tarball containing:

- `rootfs`: userspace code including business logic.

- `bootfs`: kernel and related code.
  Given security concerns (new vulnerabilities can be found)
  it is probable that the kernel will be upgraded, and as
  soon as an upgrade is available, the `bootfs` would need
  to make its way to the users running any past version.

- `bootloader`: bootloader data, if the bootloader is expected to be
  upgraded. Given that the bootloader data is small,
  it shouldn't hurt to have it provided.

It is kind of cool to have OTA packages that also work as factory
programming payloads, so sometime some extra data is included,
which is useless for OTA, for example partition label data,
and mass storage map.

It is usually frowned upon to have executable data
in an upgrade package, rather the new system includes
code that, at the first boot, will perform potential
data migrations between any past version and the new
version.

It is though possible that we want to limit backward
compatibility and the new package contains information
about suitable baseline versions, forcing the user to
perform several upgrades but ensuring a known upgrade path.



Components
##########


HTTP Server
***********

The HTTP API is simple:

- `GET /version`: can be implemented so as to retrieve
  the active/inactive slot version

- `POST /data`: send upgrade package

- `POST /go`: trigger reboot


Bootloader Upgrade
******************

Helpers are provided for:

- eMMC boot: when the system uses native eMMC boot,
  two copies of the bootloader can be stored,
  and the eMMC supports a mechanism to perform an atomic
  slot swap.

- MBR boot: when the system ROM loads a boot loader
  from an MBR boot partition, and it is possible to have
  several bootloader partitions with only one is active
  at a time, we can write the bootloader to the inactive slot
  then update the MBR


Signed Package Verification
***************************

- CMS signed package verification is provided.
  The verification features are linked to OpenSSL
  CMS signature/verification features.



Archives
********

Not all common archive formats can be extracted using purely
sequential reads.

One suitable archive structure could essentially look like this::

   - name: part0-name
     size: part0-payload-size
     contents: part0-payload

   - name: part1-name
     size: part1-payload-size
     contents: part1-payload

With the name being understandable by the custom `snafu` platform
implementation (probably `rootfs`, `bootfs` or `bootloader`).


An archive looking as follows could also be consumed sequentially::

   header:
   - name: part0-name
     size: part0-payload-size
     offset: part0-payload-offset

   - name: part1-name
     size: part1-payload-size
     offset: part1-payload-offset

   data:
   - part0-payload
   - part1-payload

And instead of encapsulating the whole package with something
providing integrity checking (or a cryptographic signature),
the header entries could be augented to include their part hashes,
and signed separately from the data.


Implemented archive formats:

- `imagewalker_tar`: Image stream as a tarball.

  A good old TAR does the job nicely, so there is a tar image walker module
  providing iteration on uncompressed tarball contents.

  Pro: can be extracted using standard tools.
  Pro: in Python standard library.
  Con: large header, padding.
  Con: too much information in header.

- `imagewalker`: Image stream as trivial key/value header followed by image data.

  ::

     name: image1-name\n
     size: image1-size\n
     \n
     <image1-data>
     name: image2-name\n
     size: image2-size\n
     \n
     <image2-data>

  Pro: simple format, similar to HTTP or e-mail.
  Con: ad-hoc format.


Versioning
**********

Providing comprehensive reporting of the system version including on
the inactive slot (or *candidate* slot, when it has been written to
but not switched over to) requires having a way to inspect a slot's
version and if the slot is made of several elements (typically
partitions) this means that either the partitions contain the version,
or that some extra data contains the version.

One way of providing extra data for versioning is to include
mini-partitions containing the version data, around individual
partitions of a slot.

For example::

   - beg-a
   - bootfs-a
   - rootfs-a
   - end-a
   - beg-b
   - bootfs-b
   - rootfs-b
   - end-b

In the above example, if the updater is interrupted in the process of
writing a partition, it is possible to know that the slot has an
invalid payload by comparing the `beg` and `end` partition contents,
and seeing that they differ.


Design Trade-offs
#################


Pros and Cons of Zero-Copy Architecture
***************************************

Pro: This means that the size of the upgrade package can be larger
than the available free space in RAM or in R/W mass storage,
which is possible if optimizations are sought
(eg. using cheaper components, or trading off space for quality).

Alternatively, and `snafu` doesn't do it, space concerns
could be dealt with by providing random access to remote resources,
eg. presenting them as a filesystem (FUSE mount) or performing
random accesses otherwise (eg. simple wrapping of HTTP range GET),
but this means more complexity in the firmware updater.

Con: This is constraining the upgrade package data format,
because the data is only ever read once and in one direction.

Con: Package verification can fail after data has been written
to the inactive slot (“ask for forgiveness, not permission”),
which means the inactive slot can't be switched over to
as a fallback mechanism in the case of eg. bit rot on the
active slot.
It is still possible to reuse `snafu` code in order to first dump the
upgrade package locally, then process it with a dry run, then if it
validated, to process it for real.

Con: Due to the sequential access to upgrade data, the upgrade package
can't contain Forward Error Correction data.
If network connectivity is very bad, such that a TCP stream covering
the transfer of the OTA package cannot be uninterrupted, then the
firmware upgrade will have trouble to happen.
It is possible to encapsulate the `snafu` transfers with a layer that
adds reliability, but it won't be as transfer-efficient as having
sufficient free space for the upgrade package containing FEC, plus
space for the reconstructed upgrade package.
It is though possible to transfer the upgrade package using something
else than `snafu`, then run `snafu` locally from the reconstructed
upgrade package.
Less transfer-efficient but more space-efficient is to transfer parts
of the upgrade package, and reconstruct it progressively so that there
is only local need for one full copy plus a chunk that is being
reconstructed.


Comparison to Other OTA Updaters
********************************

Use `snafu` when there is a strong availability constraint of
network transfers, RAM or storage space, but the local media can be
trusted (eg. eMMC).

Use `snafu` when Python is installed on the system (the `snafu` code
is very small itself, but adding the Python dependency can be a problem).


RAUC
====

https://github.com/rauc/rauc

RAUC OTA package format is either:

- *plain* format, which packages a squashfs archive, a detached CMS
  signature

- *verity* format, which packages a squashfs archive, dm-verity hash
  tree, CMS signature over a manifest.

RAUC is verifying the upgrade data, then mounting it.

RAUC can do HTTP Streaming.


SWUpdate
========

https://swupdate.org/ / https://github.com/sbabic/swupdate

SWUpdate package format is using CPIO with a metadata header (`sw-description`):
https://github.com/sbabic/swupdate/blob/master/doc/source/sw-description.rst

SWUpdate uses a detached signature `sw-description.sig` in the case of signing:
https://github.com/sbabic/swupdate/blob/master/doc/source/swupdate.rst#what-is-expected-from-a-swupdate-run
The header contains hashes for the bundled images, so the check for
images needs to be done separately.

SWUpdate requires a local copy when installing from the network:
https://github.com/sbabic/swupdate/blob/master/doc/source/swupdate.rst#images-fully-streamed
The accesses are still sequential, so it is possible to adapt SWUpdate
to operate over a filesystem-to-network-stream proxy.

SWUpdate has lots of features (different bootloaders, image types).


License
#######

`LICENSES/MIT.txt` in this repo
