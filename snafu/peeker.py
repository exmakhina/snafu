#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Input stream peeker

from typing import Tuple
import logging


logger = logging.getLogger(__name__)


class FakeBytes:
	def __init__(self, fi):
		self._fi = fi
		self._buf = b""
		self._len = 0

	def consume_up_to(self, pos):
		#logger.debug("Consuming up to %s", pos)
		delta = pos - self._len
		self._buf += self._fi.read(delta)
		self._len = pos

	def __getitem__(self, x):
		if isinstance(x, int):
			#logger.debug("getitem int %s", x)
			if x >= self._len:
				self.consume_up_to(x+1)
		elif isinstance(x, slice):
			#logger.debug("getitem slice %s", x)
			if x.stop > self._len:
				self.consume_up_to(x.stop)
		else:
			raise NotImplementedError(x)
		return self._buf[x]
