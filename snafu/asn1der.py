#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# ASN.1 DER stream parsing helper

"""

Many thanks to the Let's Encrypt fly-through on ASN.1 DER:
https://letsencrypt.org/docs/a-warm-welcome-to-asn1-and-der/

"""

from typing import Tuple
import logging


logger = logging.getLogger(__name__)


def parse(der: bytes, pos: int) -> Tuple[int, int]:
	"""
	:param der: DER structure
	:param pos: position in structure

	:return: header length and payload length
	"""
	first = der[pos + 1]
	if der[pos + 1] & 0x80 == 0:
		length = first
		hl = 2
	else:
		lengthbytes = first & 0x7F
		length = int.from_bytes(der[pos + 2: pos + 2 + lengthbytes], "big")
		hl = 2 + lengthbytes
	return hl, length


class Node:
	def __init__(self, der, pos, parent=None):
		self._parent = parent
		self._der = der
		self._pos = pos
		self._hl, self._len = parse(der, pos)

	def __str__(self):
		return f"(Node pos={self._pos} hl={self._hl} len={self._len})"

	def next(self) -> "Node":
		pos_ = self._pos + self._hl + self._len
		if self._parent is not None:
			parent = self._parent
			if pos_ >= parent._pos + parent._hl + parent._len:
				raise RuntimeError(f"{self} next would go beyond parent {parent}")

		return Node(self._der, pos_, parent=self._parent)

	def child(self) -> "Node":
		return Node(self._der, self._pos + self._hl, parent=self)

	def __getitem__(self, idx):
		x = self.child()
		for i in range(idx):
			x = x.next()
		return x

	def t(self):
		return self._der[self._pos]

	def l(self):
		return self._len

	def v(self):
		return self._der[self._pos+self._hl:self._pos+self._hl+self._len]

	def vpos(self):
		return self._pos+self._hl

	def tlv(self):
		return self._der[self._pos:self._pos+self._hl+self._len]

	def lv(self):
		return self._der[self._pos+1:self._pos+self._hl+self._len]

