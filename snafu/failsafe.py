#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Fail-safe assertion / verification

"""

This module provides the this_is_fail_safe() function which can be called:

- to assert that if the system crashes when this function is called,
  the software will handle it properly.
- to selectively cause system crashes in order to verify fail-safe behavior


A simple text database at :envar:`XM_FAILSAFE_DB` contains __name__:line on each line,
but `call_selected_for_verification()` can be overridden to select call sites
differently.


"""

import sys
import io
import os
import logging
import time
from typing import Optional


logger = logging.getLogger(__name__)


test_by_locus = {
}


failsafe_db = os.environ.get("XM_FAILSAFE_DB", "/etc/xm_failsafe.cfg")
if os.path.exists(failsafe_db):
	with io.open(failsafe_db, "r", encoding="utf-8") as fi:
		for line in fi:
			if line.startswith("#"):
				continue
			n, fl = line.rsplit(":", 1)
			fl = int(fl)
			test_by_locus.add((n, fl))


def tested_by_locus(n, l):
	if (n,l) in test_by_locus:
		return f"{n}:{l}"


die_delay = 5


def die():
	"""
	Crash the system violently
	"""
	fd = os.open("/proc/sysrq-trigger", os.O_WRONLY)
	os.write(fd, b"c\n")
	os.close(fd)


def call_selected_for_verification() -> Optional[str]:
	"""
	:return: reason if call was selected for verification

	Note: this function must only be called by this_is_fail_safe(),
	so the real caller is 2 frames above.
	"""
	fr = sys._getframe(2)
	l = fr.f_lineno
	n = fr.f_globals["__name__"]
	return tested_by_locus(n,l)


def this_is_fail_safe(*args, **kw):
	"""
	Assertion or verification that the system can crash,
	when this function is called,
	and the crash recovery will work as intended.
	"""
	if reason := call_selected_for_verification():
		logger.info("Verifying that “%s” is fail-safe, goodbye!", reason)
		time.sleep(die_delay)
		die()
	else:
		logger.debug("Developer asserted a fail-safe condition")
