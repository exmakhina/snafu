#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# tar image stream walker

import tarfile
import logging

from .unwrapping import LimitedInputStream


def walk(fi):
	with tarfile.open(mode="r|",
	  fileobj=fi,
	  format=tarfile.PAX_FORMAT,
	  ) as pkg_tar:
		for tarinfo in pkg_tar:
			stream = LimitedInputStream(pkg_tar.fileobj, tarinfo.size, 512)
			yield tarinfo.name, tarinfo.size, stream
