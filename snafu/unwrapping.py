#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Unwrapping helpers

import logging


logger = logging.getLogger(__name__)


class LimitedInputStream(object):
	def __init__(self, fi, size, name=None):
		self.fi = fi
		self.size = size
		self._pos = 0
		if name is None:
			name = f"(stream size={size} fi={fi})"
		self._name = name

	def __str__(self):
		return f"{self._name} @ {self._pos}"

	def __enter__(self):
		return self

	def __exit__(self, *args):
		if self._pos != self.size:
			logger.error("%s has only processed %d of %d, probable bug!",
			 self._name,
			 self._pos, self.size)
			self.read(self.size - self._pos)

	def tell(self):
		return self._pos

	def read(self, length=None):
		if length is None:
			length = self.size - self._pos

		cur = self._pos
		nex = min(self.size, cur + length)
		actual = nex - cur
		data = self.fi.read(actual)
		self._pos += len(data)
		return data
