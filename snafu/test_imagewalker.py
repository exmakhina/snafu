#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Helpers

import io
import logging


logger = logging.getLogger(__name__)


def test_walk():
	from .imagewalker import walk
	x = io.BytesIO()
	for i in range(4):
		x.write(f"name: pouet {i}\n".encode())
		x.write(b"size: 5\n")
		x.write(b"\n")
		x.write(b"12345")
	x.seek(0)

	for name, size, stream in walk(x):
		logger.info("Processing %s of size %d", name, size)
		with stream:
			stream.read()
