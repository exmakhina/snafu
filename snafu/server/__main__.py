#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Server launcher

import sys, os
import logging
import faulthandler
import signal
import configparser
import socket
import importlib


logger = logging.getLogger()


def main():
	import argparse

	parser = argparse.ArgumentParser(
	 description="snacfu server",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--log-to-journald",
	 action="store_true",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)

	subp = subparsers.add_parser(
	 "http-server",
	 help="Run HTTP server",
	)

	subp.add_argument("--platform",
	 required=True,
	 help="Target platform",
	)

	subp.add_argument("--config",
	 required=True,
	 help="Path to config file (.ini).",
	)

	subp.add_argument("--port",
	 type=int,
	 help="Port to use (use -1 for systemd socket activation)",
	)

	def do_image_server(args):
		from socketserver import TCPServer
		from .httpd import ApiHTTPHandler
		config = configparser.ConfigParser()
		config.read(args.config)
		server = TCPServer(("localhost", args.port), ApiHTTPHandler, bind_and_activate=False)

		server.config = config
		module = f"snafu.platform.{args.platform}"
		platform_module = importlib.import_module(module)
		server.platform_module = platform_module

		platform_module.initialize()

		if args.port == -1:
			server.socket = socket.fromfd(
			 fd=3, # SD_LISTEN_FDS_START, see man sd_listen_fds
			 family=server.address_family,
			 type=server.socket_type,
			)
		else:
			server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
			server.server_bind()
			server.server_activate()

		if "NOTIFY_SOCKET" in os.environ:
			import systemd.daemon
			res = systemd.daemon.notify("READY=1")
			if not res:
				logger.error("Couldn't notify of completed start-up")

		server.serve_forever()

	subp.set_defaults(func=do_image_server)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	if args.log_to_journald:
		import systemd
		import systemd.journal
		handler = systemd.journal.JournalHandler(
		 SYSLOG_IDENTIFIER="snafu-server",
		)
		logging.root.addHandler(handler)
		logging.root.setLevel(getattr(logging, args.log_level))
	else:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	faulthandler.register(signal.SIGHUP, file=sys.stderr, all_threads=True, chain=False)

	if getattr(args, 'func', None) is None:
		parser.print_help()
		return 1
	else:
		return args.func(args)


if __name__ == "__main__":
	ret = main()
	raise SystemExit(ret)
