#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# HTTP handler

from http.server import BaseHTTPRequestHandler
import traceback
import json
import logging
import urllib.parse


logger = logging.getLogger(__name__)


def process_data(self):
	root = self.server.platform_module.process_data(self)
	code = 200
	content_type = "application/json"
	body = json.dumps(root).encode('utf-8')
	return code, content_type, body


def process_version(self):
	root = self.server.platform_module.process_version(self)
	code = 200
	content_type = "application/json"
	body = json.dumps(root).encode('utf-8')
	return code, content_type, body


def process_go(self):
	root = self.server.platform_module.process_go(self)
	code = 200
	content_type = "application/json"
	body = json.dumps(root).encode('utf-8')
	return code, content_type, body


class ApiHTTPHandler(BaseHTTPRequestHandler):
	"""
	"""
	def do_all(self, method="GET"):
		parseresult = urllib.parse.urlparse(self.path)
		path = parseresult.path

		logger.info("Handling %s %s %s", method, self.path, list(self.headers.items()))

		code = 200
		content_type = "application/json"
		reply = json.dumps('Success').encode('utf-8')

		if path == "/data" and method == "POST":
			try:
				code, content_type, reply = process_data(self)
			except PermissionError as e:
				code, reply = 403, self.handle_error(e, 403, "Forbidden")
			except Exception as e:
				code, reply = 500, self.handle_error(e)

		elif path.startswith("/version") and method == "GET":
			try:
				code, content_type, reply = process_version(self)
			except Exception as e:
				code, reply = 500, self.handle_error(e)

		elif path == "/go" and method == "POST":
			try:
				code, content_type, reply = process_go(self)
			except PermissionError as e:
				code, reply = 403, self.handle_error(e, 403, "Forbidden")
			except Exception as e:
				code, reply = 500, self.handle_error(e)

		else:
			logger.warning("404: method=%s path=%s", method, path)
			code, content_type, reply = 404, "text/plain", \
			 "Not found: {} {}\n" \
			 .format(method, path).encode("utf-8")

		try:
			self.send_response(code)
			self.send_header("Content-type", content_type)
			self.send_header("Content-Length", len(reply))
			self.end_headers()
			self.wfile.write(reply)
		except (BrokenPipeError, ConnectionResetError):
			logger.info("Client stopped reading!")


	def do_GET(self):
		self.do_all("GET")

	def do_PUT(self):
		self.do_all("PUT")

	def do_POST(self):
		self.do_all("POST")

	def do_HEAD(self):
		self.do_all("HEAD")

	def do_OPTIONS(self):
		self.do_all("OPTIONS")

	def handle_error(self, e, code=500, desc="Internal error"):
		msg = "%s: %s"
		args = (desc, e,)

		level = logging.ERROR
		extra = None
		exc_info = True
		stack_info = False
		if logger.isEnabledFor(level):
			logger._log(level, msg, args, exc_info=exc_info, extra=extra, stack_info=stack_info)

		err = dict(
		 error=msg % args,
		 stack_trace=traceback.format_exc(),
		)

		ret = json.dumps(err).encode("utf-8")

		return ret
