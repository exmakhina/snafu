#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# eMMC booting tools

import io
import logging
import subprocess
import re
import contextlib

from ..system import (
 stream_to_disk,
 run,
)


logger = logging.getLogger(__name__)


def emmc_bootslot_get(mmc_dev):
	"""
	Find current bootslot on mmc
	"""
	# This is less code than doing an IOCTL
	cmd = [
	 "mmc", "extcsd", "read", f"/dev/{mmc_dev}",
	]

	for line in run(cmd, stdout=subprocess.PIPE).stdout.splitlines():
		line = line.rstrip()
		m = re.match(rb"^ Boot Partition (?P<part>1|2) enabled$", line)
		if m is None:
			continue
		return int(m.group("part")) - 1

	raise RuntimeError(f"MMC {mmc_dev} doesn't have boot enabled")


def emmc_bootslot_set(mmc_dev, slot: int):
	"""
	"""
	cmd =[
	 "mmc",
	 "bootpart",
	 "enable",
	 "1" if slot == 0 else "2",
	 "1", # TODO FIXME
	 f"/dev/{mmc_dev}",
	]
	run(cmd)


@contextlib.contextmanager
def unforce_ro(mmc_dev, slot):
	with io.open(f"/sys/block/{mmc_dev}boot{slot}/force_ro", "wb") as fo:
		fo.write(b"0")
	yield
	with io.open(f"/sys/block/{mmc_dev}boot{slot}/force_ro", "wb") as fo:
		fo.write(b"1")


def emmc_bootloader_update(mmc_dev, fi, size):
	"""
	Update bootloader on eMMC

	:return: whether the update was necessary

	Note: both partitions are enabled and there is no fallback,
	as it would be in the first-level bootloader.
	"""

	slot_cur = emmc_bootslot_get(mmc_dev)
	slot_nxt = 1 - slot_cur

	with unforce_ro(mmc_dev, slot_nxt):
		path = f"/dev/{mmc_dev}boot{slot_nxt}"
		stream_to_disk(fi, size, path)

	return slot_nxt


__all__ = (
 "emmc_bootloader_update",
)
