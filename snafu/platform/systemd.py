#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# systemd tools

import time
import logging

from ..system import (
 run,
)


logger = logging.getLogger(__name__)


def delayed_reboot(method="reboot", delay=10):

	logger.debug("Performing delayed %s in %f", method, delay)

	when = int((time.time() + delay) * 1e6)
	cmd = [
	  "busctl",
	  "call",
	  "org.freedesktop.login1",
	  "/org/freedesktop/login1",
	  "org.freedesktop.login1.Manager",
	  "ScheduleShutdown",
	  "st",
	  method,
	  "{}".format(when)
	 ]
	run(cmd)


__all__ = (
 "delayed_reboot",
)
