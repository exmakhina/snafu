#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# Helpers for u-boot bootloader

import logging
import subprocess

from ..system import run

logger = logging.getLogger(__name__)

# TODO quoting


def fw_setenv(name, value):
	"""
	Set a uboot environment variable.
	"""
	cmd = [
	 "fw_setenv",
	 name,
	 value,
	]
	run(cmd)


def fw_getenv(name, default=None):
	"""
	Get the value of a uboot environment variable.
	"""
	cmd = ["fw_printenv", name]
	line = run(cmd, stdout=subprocess.PIPE).stdout.rstrip()
	k, v = line.decode("utf-8").split("=", 1)
	assert k == name

	if not v:
		if default is None:
			raise KeyError(name)
		return default

	return v

__all__ = (
 "fw_getenv",
 "fw_setenv",
)
